tutorial: https://www.youtube.com/watch?v=J9sfR6HN6BY

vscode extensions:
ES7+ React/Redux/React-Native
JavaScript and TypeScript Nightly
Tailwind CSS IntelliSense
Prisma

npx create-next-app@13.4.19

prompts:
--
Projectname issue-tracker
TypeScript Yes
ESLint Yes
Tailwind CSS Yes
src/ directory No
App Router Yes
customize default import alias No

cd issue-tracker
npm run dev

http://localhost:3000

## react-icons.github.io/react-icons/
npm install react-icons --save

npm i classnames@2.3.2 ---> for condition expressions


## create mysql database
docker run --name mysql-container -e MYSQL_ROOT_PASSWORD=MyPassword -d -p 3306:3306 mysql
docker exec -it mysql-container mysql -u root -p
mysql> CREATE DATABASE issue_tracker;

## datagrip like dbeaver

npm i prisma@5.3.1 ---> for db connectivity
npx prisma init
## After creating model in schema.prisma file., below command will generate tables data for model created.
npx prisma format
npx prisma migrate dev  ---> input migrtion name: create issue

## API requst data validation 
npm i zod@3.22.2

##to test using postman 
http://192.168.1.2:3000/api/Issues   
POST --body -- raw -- json --
{
    "title": "first issue",
    "description": "description of first issue"
}




